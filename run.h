#ifndef RUN
#define RUN
//関数のプロトタイプ宣言
extern	int beforeStart();
extern int execStart(Ev3SeqParam param);
extern int afterStart();
extern int terminateStart(Ev3SeqParam param);

extern int execOnboard(Ev3SeqParam param);
extern int terminateOnboard(Ev3SeqParam param);

extern int beforeTrace();
extern int execTrace(Ev3SeqParam param);
extern int afterTrace();
extern int terminateTrace(Ev3SeqParam param);

extern	int beforePrize();
extern int execPrize(Ev3SeqParam param);
extern int afterPrize();
extern int terminatePrize(Ev3SeqParam param);

extern int beforePutprize();
extern int execPutprize(Ev3SeqParam param);
extern int terminatePutprize(Ev3SeqParam param);

extern int beforeDown();
extern int execDown(Ev3SeqParam param);
extern int afterDown();
extern int terminateDown(Ev3SeqParam param);

extern int execParking(Ev3SeqParam param);
extern int afterParking();
extern int terminateParking(Ev3SeqParam param);

extern int beforeStop();
extern int execStop(Ev3SeqParam param);
extern int terminateStop(Ev3SeqParam param);

extern int beforeLineTrace();
extern int execLineTrace(Ev3SeqParam param);
extern int terminateEncoder(Ev3SeqParam param);
extern int terminateEncoderMM(Ev3SeqParam param);
extern int terminateEncoderBackMM(Ev3SeqParam param);

extern int beforeLineTraceColor();
extern int execLineTraceColor(Ev3SeqParam param);
extern int terminateColor(Ev3SeqParam param);

extern int beforeGoStraightAhead();
extern int execGoStraightAhead(Ev3SeqParam param);
extern int terminateLineDetect(Ev3SeqParam param);

extern int beforeRotate();
extern int execRotate(Ev3SeqParam param);
extern int terminateRotate(Ev3SeqParam param);

extern int beforeLineReturn();
extern int execLineReturn(Ev3SeqParam param);
extern int afterLineReturn();
extern int terminateLineReturn(Ev3SeqParam param);


void distanceUpdate();
void directionUpdate();
void lineDetectUpdate();
void toBoard();
void train_wait();
int16_t max(int16_t a,int16_t b);
int16_t min(int16_t a,int16_t b);
float round_e(float num);
hsv_raw_t convertRGB2HSV(rgb_raw_t src);
colorid_t get_floor_color(sensor_port_t port);
rgb_raw_t conv_hsv_hue(rgb_raw_t rgb);
colorid_t ev3_color_sensor_get_color_nice(sensor_port_t port);
int distance_cal(int8_t distance);
void xcm_GO(int8_t distance, uint32_t speed);
void xcm_GO_ver2(int8_t distance, uint32_t speed);
void tire_position_reset(void);
void oshidashi();
void iron_tail(int8_t num);
void color_catch(void);
void fanfale(void);
void fanfale2(void);
#endif /* RUN */
