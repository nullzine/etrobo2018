#include "ev3api.h"
#include "app.h"
#include "variable.h"
#include "run.h"
#include "math.h"
/*
** Variables
*/
#define TIRE_DIAMETER 81.0  //タイヤ直径（81mm）
#define TREAD 132.6			// 車体トレッド幅(132.6mm)
#define ANGLE_CORRECTION 18	// 回りすぎるので補正を掛ける

int is_train_wait; //プラレールの通過待ち状態終了
int is_prize_finished;  // 懸賞処理終了フラグ
int is_putprize_finished; //懸賞置く処理終了フラグ

float distance;					// 走行距離[mm]
float distanceL, distanceR;		// 各タイヤの3ms間の移動距離
float pre_angleL, pre_angleR;	// 1つ前のタイヤの回転量
float direction;				// 回転角度[°]

int line_return_state;			// ライン復帰用変数
float before_black_dist;
float after_black_dist;
int return_edge;

int abs_i(int num){
	if(num>=0)
		return num;
	else
		return num*(-1);
}
float abs_f(float num){
	if(num>=0)
		return num;
	else
		return num*(-1);
}

/*
** Main functions
*/
/*
 ******************************************** Start *********************************************
*/
int beforeStart(){
    int8_t steer_value = 0; //ハンドルを切る値
	int8_t prev = 0; //1つ前の反射値
	int8_t diff = 0; //反射値の差
	int is_train_wait = 0; //プラレールの通過待ち状態終了
	float integral = 0; //誤差の累積
	color_sensor_mode = COLOR_MODE;			// カラーセンサのモードをカラーモードにする
	return TRUE;
}

int execStart(Ev3SeqParam param){
    integral += (ltvalue.threshold_value - ltvalue.ref_value) * 0.002; //I制御 誤差をたたきこむ
	diff = (ltvalue.ref_value - prev) / 0.002;    //前の反射値との差を計算

    //if (!is_train_wait && 1 <= ccvalue.curve_count && ccvalue.switch_curv) {
	if (!is_train_wait ) {
		train_wait();
		is_train_wait = 1;
		psvalue.isContact = 0;
	}
	steer_value = ((ltvalue.threshold_value - ltvalue.ref_value) * 0.8)
			+ integral * 2 + (diff / 1.3);
	ev3_motor_steer(left_motor, right_motor, 20, steer_value * 2);
	time++;

    return TRUE;
}

int afterStart(){
    //ltvalue.threshold_value = (ltvalue.black_value + BOARD_VALUE) / 2;
	ltvalue.threshold_value = (ltvalue.black_value + color_param[5]) / 2;	//(20170711 kashiwaba変更)
	toBoard();
	ev3_motor_rotate(left_motor, -30, 20, false); //板の上のライントレース復帰安定向上
	ev3_motor_rotate(right_motor, 30, 20, true);
    return TRUE;
}

int terminateStart(Ev3SeqParam param){
    // Terminate
    int ret = FALSE;
    if (is_train_wait) {
	    ret = TRUE;
	}
    return ret;
}


/*
 ********************************************* Onboard *********************************************
*/
int execOnboard(Ev3SeqParam param){
    integral += (ltvalue.threshold_value - ltvalue.ref_value) * 0.002; //I制御 誤差をたたきこむ
	diff = (ltvalue.ref_value - prev) / 0.002;    //前の反射値との差を計算

    steer_value = ((ltvalue.threshold_value - ltvalue.ref_value) * 2.4);
	ev3_motor_steer(left_motor, right_motor, SPEED_BOARD, steer_value * 2);
	//floor_color = ev3_color_sensor_get_color_nice(color_sensor); //カラーセンサーで床の色を取り続ける
	floor_color = get_floor_color(color_sensor);				   //カラーセンサーで床の色を取り続ける(20170711 kashiwaba変更)

	switch(floor_color){
		case COLOR_BLUE:
		ev3_lcd_draw_string("BLUE  ", 0, CALIB_FONT_HEIGHT*3);
		break;
		case COLOR_RED:
		ev3_lcd_draw_string("RED   ", 0, CALIB_FONT_HEIGHT*3);
		break;
		case COLOR_YELLOW:
		ev3_lcd_draw_string("YELLOW", 0, CALIB_FONT_HEIGHT*3);
		break;
		case COLOR_GREEN:
		ev3_lcd_draw_string("GREEN ", 0, CALIB_FONT_HEIGHT*3);
		break;
		case COLOR_BLACK:
		ev3_lcd_draw_string("BLACK ", 0, CALIB_FONT_HEIGHT*3);
		break;
		case COLOR_WHITE:
		ev3_lcd_draw_string("WHITE ", 0, CALIB_FONT_HEIGHT*3);
		break;
	}

	if ((floor_color == COLOR_BLUE) || (floor_color == COLOR_RED)
			|| (floor_color == COLOR_YELLOW)
			|| (floor_color == COLOR_GREEN)) {
		color_catch();
	}
	if (psvalue.exit_flag) {
		if(boardflags == 0){
			//プラレールを検知する場所まで(右モータ40cm)ライントレース
			if ((ev3_motor_get_counts(right_motor) > distance_cal(40))) {
				xcm_GO(5, SPEED_BOARD);
				train_wait();
				psvalue.exit_flag = 0;
				//xcm_GO(40, 50); //40cm前進
				ev3_motor_rotate(left_motor,distance_cal(40),50,false);
				ev3_motor_rotate(right_motor,distance_cal(41),51,true);

				ev3_motor_stop(left_motor, true);
				ev3_motor_stop(right_motor, true);

				ev3_motor_rotate(left_motor, -30, 20, false); //板の上のライントレース復帰安定向上
				ev3_motor_rotate(right_motor, 30, 20, true);

				boardflags = 1;
			}
		} else {
			//プラレールを検知する場所まで(右モータ40cm)ライントレース
			if ((ev3_motor_get_counts(right_motor) > distance_cal(20))) {
				xcm_GO(5, SPEED_BOARD);
			}
		}
	}
    return TRUE;
}

int terminateOnboard(Ev3SeqParam param){
    // Terminate
    int ret = FALSE;

    if (psvalue.exit_flag) {
		if(boardflags != 0){
			if (ev3_motor_get_counts(right_motor) - ev3_motor_get_counts(left_motor) > 180) {
			//if ((ev3_motor_get_counts(right_motor) > distance_cal(25))) {
				ret = TRUE;

			}
		}
	}
    return ret;
}
/*
 ********************************************* Trace *********************************************
 */
int beforeTrace(){
	ev3_led_set_color(LED_GREEN );
	tire_position_reset();
	return TRUE;
}

int execTrace(Ev3SeqParam param){
	//板の真ん中までライントレース
	steer_value = ((ltvalue.threshold_value - ltvalue.ref_value) * 2.4);
	ev3_motor_steer(left_motor, right_motor, SPEED_BOARD, steer_value * 2);
	return TRUE;
}

int afterTrace(){
	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);
    ev3_led_set_color(LED_OFF);
	return TRUE;
}

int terminateTrace(Ev3SeqParam param){
	int ret = FALSE;
	if ((ev3_motor_get_counts(right_motor) > distance_cal(17))) {
		ret = TRUE;
	}
	return ret;
}
/*
 ********************************************* Prize *********************************************
*/
int beforePrize(){
    is_prize_finished = FALSE;
    return TRUE;
}

int execPrize(Ev3SeqParam param){

    ev3_motor_rotate(left_motor, 155, 40, false);      //90°ターン
	ev3_motor_rotate(right_motor, -155, 40, true);     //90°ターン

	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);

	//xcm_GO(29, 18);
	xcm_GO_ver2(29, 18);

	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);
	//アームを一番上まで上げる
	ev3_motor_rotate(arm_motor, ARM_UP, 40, true);

	//xcm_GO(-29, 18);
	xcm_GO_ver2(29, -18);

	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);

	ev3_motor_rotate(right_motor, 144, 40, false);      //90°ターン
	ev3_motor_rotate(left_motor, -144, 40, true);     //90°ターン

	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);

	//xcm_GO(-5, 18);
	xcm_GO_ver2(5, -18);


	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);


	//アームを下げる
	ev3_motor_rotate(arm_motor, -ARM_UP, 40, true);

	train_wait();

	//アームを一番上まで上げる
	ev3_motor_rotate(arm_motor, 88, 25, true);

	psvalue.exit_flag = 0;
	tire_position_reset();
	ev3_motor_rotate(left_motor,distance_cal(60),30,false);
	ev3_motor_rotate(right_motor,distance_cal(60),30,true);
    is_prize_finished = TRUE;

    return TRUE;
}

int afterPrize(){
    ev3_led_set_color(LED_ORANGE);
	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);
    return TRUE;
}

int terminatePrize(Ev3SeqParam param){
    int ret = FALSE;
    if (is_prize_finished){
         ret = TRUE;
    }
    return ret;
}

/*
 ********************************************* Putprize *********************************************
 */
int beforePutprize(){
    is_putprize_finished = FALSE;
    return TRUE;
}

int execPutprize(Ev3SeqParam param){
	tslp_tsk(800);
	ev3_motor_rotate(left_motor, 155, 30, false);      //90°ターン
	ev3_motor_rotate(right_motor, -155, 30, true);     //90°ターン

	tslp_tsk(800);

	//xcm_GO(10, 18);
	xcm_GO_ver2(10, 18);

	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);

	//アームを下まで下げる
	ev3_motor_rotate(arm_motor, -60, 15, true);

	//xcm_GO(-10, 18);
	xcm_GO_ver2(12, -18);

	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);

	ev3_motor_rotate(left_motor, -155, 30, false);      //90°ターン
	ev3_motor_rotate(right_motor, 155, 30, true);     //90°ターン

	is_putprize_finished = TRUE;

	return TRUE;
}

int terminatePutprize(Ev3SeqParam param){
	int ret = FALSE;
	if (is_putprize_finished){
	         ret = TRUE;
	}
	return ret;
}

/*
 ********************************************* Down *********************************************
*/
int beforeDown(){
	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);
	tire_position_reset();
	return TRUE;
}

int execDown(Ev3SeqParam param){
    integral += (ltvalue.threshold_value - ltvalue.ref_value) * 0.002; //I制御 誤差をたたきこむ
	diff = (ltvalue.ref_value - prev) / 0.002;    //前の反射値との差を計算

    ltvalue.threshold_value = (ltvalue.black_value + TAPE_VALUE) / 2; //板を降りたとき閾値を変更
	//右折したか検知
	if (ev3_motor_get_counts(left_motor) - ev3_motor_get_counts(right_motor) < 250) {//右と左のタイヤの角度の差が250度以下
		if(ev3_motor_get_counts(left_motor) < distance_cal(15)){//板を下りてから進んだ距離が15cm未満
			if(ltvalue.ref_value < ltvalue.threshold_value){//反射値が閾値より小さい（黒のとき）
				steer_value = ((ltvalue.threshold_value - ltvalue.ref_value) * 1.5)*1.3; // P値大 曲がりやすくする
				ev3_motor_steer(left_motor, right_motor, 26, steer_value * 2);
			}else{						//白のとき
				steer_value = ((ltvalue.threshold_value - ltvalue.ref_value) * 0.9)*1.3; // P値小 曲がりづらくする
				ev3_motor_steer(left_motor, right_motor, 26, steer_value * 2);
			}
		}else{							//15cm以上進んだ後
			steer_value = ((ltvalue.threshold_value - ltvalue.ref_value) * 1.5);  //右折用のライントレース
			ev3_motor_steer(left_motor, right_motor, 24, steer_value * 2);
		}
	}
    return TRUE;
}

int afterDown(){
    ev3_led_set_color(LED_RED);
	tire_position_reset();	//角位置リセット
    return TRUE;
}

int terminateDown(Ev3SeqParam param){
    // Terminate
    int ret = FALSE;
    if (ev3_motor_get_counts(left_motor) - ev3_motor_get_counts(right_motor) >= 250){
         ret = TRUE;
    }
    return ret;
}


/*
 ********************************************* Parking *********************************************
*/
int execParking(Ev3SeqParam param){
    if ((ev3_motor_get_counts(left_motor) < distance_cal(40))
			|| (ev3_motor_get_counts(right_motor) < distance_cal(40))) { // 40cm進んだか
		steer_value = ((ltvalue.threshold_value - ltvalue.ref_value) * 1.5);
		ev3_motor_steer(left_motor, right_motor, 26, steer_value * 2);
	}
    return TRUE;
}

int afterParking(){
    ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);
	fanfale();
	ev3_stp_cyc(MOTOR_TASK);
	ev3_stp_cyc(SENSOR_TASK);
	ev3_stp_cyc(MONITOR_TASK);
	ev3_stp_cyc(CONTACT_TASK);
	ev3_stp_cyc(SONOR_TASK);
    return TRUE;
}

int terminateParking(Ev3SeqParam param){
    int ret = FALSE;
    if (!((ev3_motor_get_counts(left_motor) < distance_cal(40))
			|| (ev3_motor_get_counts(right_motor) < distance_cal(40))))
    {
        ret = TRUE;
    }
    return ret;
}


/*
 ********************************* Stop *********************************
*/
int beforeStop(){
    // Exec
    ev3_lcd_draw_string("STOP", 0, CALIB_FONT_HEIGHT*1);
    return TRUE;
}

int execStop(Ev3SeqParam param){
    // Exec
    ev3_motor_set_power(left_motor, 0);
    ev3_motor_set_power(right_motor, 0);
    return TRUE;
}

int terminateStop(Ev3SeqParam param){
    // Terminate
    int ret = FALSE;
    return ret;
}

/*
 ******************************************** LineTrace *********************************************
*/
int beforeLineTrace(){
    color_sensor_mode = LUMINANCE_MODE;				// カラーセンサを輝度モードにする
	distance = 0.0;									// 距離を初期化
	pre_angleL = ev3_motor_get_counts(left_motor);	// モータの過去値を初期化
    pre_angleR = ev3_motor_get_counts(right_motor);
    return TRUE;
}

int execLineTrace(Ev3SeqParam param)
{
	// 前回偏差値を退避
	prev = diff;
	// 今回偏差値を算出
	diff = ltvalue_luminance.ref_value - ltvalue_luminance.threshold_value;
	// 偏差の累積値を算出
	integral += (diff + prev) / 2.0 * 0.003;
	// ステアリング量を計算
	steer_value = (param.p * diff) + (param.i * integral) + (param.d * (diff - prev) / 0.003);
	// モーター駆動
	ev3_motor_steer(left_motor, right_motor, param.speed, steer_value*param.edge);

	// 距離更新
	distanceUpdate();

    return TRUE;
}

int terminateEncoder(Ev3SeqParam param)
{
	// エンコーダ値
	int32_t encoder = 0;
	encoder = (ev3_motor_get_counts(left_motor) + ev3_motor_get_counts(right_motor)) / 2;

	// エンコーダ値が指定エンコーダを超えたら終了
    if (encoder >= param.encoder)
	{
        return TRUE;
    }
    return FALSE;
}

int terminateEncoderMM(Ev3SeqParam param)
{
	// 総走行距離が指定距離を超えたら終了
    if (distance >= param.encoder)
	{
         return TRUE;
    }
    return FALSE;
}

int terminateEncoderBackMM(Ev3SeqParam param)
{
	// 総走行距離が指定距離を超えたら終了(バック)
    if (distance <= -1*param.encoder)
	{
         return TRUE;
    }
    return FALSE;
}

/*
******************************************** LineTraceColor *********************************************
*/
int beforeLineTraceColor()
{
	color_sensor_mode = COLOR_MODE;				// カラーセンサを輝度モードにする
	distance = 0.0;									// 距離を初期化
	distanceL = 0.0;
	distanceR = 0.0;
	pre_angleL = ev3_motor_get_counts(left_motor);	// モータの過去値を初期化
	pre_angleR = ev3_motor_get_counts(right_motor);
	return TRUE;
}

int execLineTraceColor(Ev3SeqParam param)
{
	// 前回偏差値を退避
	prev = diff;
	// 今回偏差値を算出
	diff = ltvalue.ref_value - ltvalue.threshold_value;
	// 偏差の累積値を算出
	integral += (diff + prev) / 2.0 * 0.003;
	// ステアリング量を計算
	steer_value = (param.p * diff) + (param.i * integral) + (param.d * (diff - prev) / 0.003);
	// モーター駆動
	ev3_motor_steer(left_motor, right_motor, param.speed, steer_value*param.edge);

	// 距離更新
	distanceUpdate();

	return TRUE;
}

int terminateColor(Ev3SeqParam param)
{
	// 白黒以外を見つけたら終了
	colorid_t floor_color = get_floor_color(color_sensor);
	if (floor_color == COLOR_RED || floor_color == COLOR_BLUE || floor_color == COLOR_GREEN || floor_color == COLOR_YELLOW)
	{
		return TRUE;
	}
	return FALSE;
}

/*
 ******************************************** Go straight ahead *********************************************
*/
int beforeGoStraightAhead(){
	distance = 0.0;									// 距離を初期化
	pre_angleL = ev3_motor_get_counts(left_motor);	// モータの過去値を初期化
    pre_angleR = ev3_motor_get_counts(right_motor);
	line_return_state = BEFORE_BLACK_DETECTION;
    return TRUE;
}

int execGoStraightAhead(Ev3SeqParam param)
{
	// モーター駆動
	ev3_motor_set_power(left_motor, param.speed);
	ev3_motor_set_power(right_motor, param.speed);
	// 距離更新
	distanceUpdate();
	// ライン検知状態更新
	lineDetectUpdate();

    return TRUE;
}

int terminateLineDetect(Ev3SeqParam param)
{
    colorid_t floor_color = get_floor_color(color_sensor);
	if (floor_color == COLOR_BLACK)
	{
		return TRUE;
	}
	return FALSE;
}

/*
******************************************** Rotate *********************************************
*/
int beforeRotate()
{
	distance = 0.0;									// 距離を初期化
	distanceL = 0.0;
	distanceR = 0.0;
	pre_angleL = ev3_motor_get_counts(left_motor);	// モータの過去値を初期化
	pre_angleR = ev3_motor_get_counts(right_motor);
	direction = 0.0;								// 現在の角度を初期化
	return TRUE;
}

int execRotate(Ev3SeqParam param)
{
	// モーター駆動
	if (param.encoder > 0){
		ev3_motor_set_power(left_motor, param.speed);
		ev3_motor_set_power(right_motor, (-1)*param.speed);
	}
	else{
		ev3_motor_set_power(left_motor, (-1)*param.speed);
		ev3_motor_set_power(right_motor, param.speed);
	}

	// 距離更新
	distanceUpdate();
	// 角度更新
	directionUpdate();

	return TRUE;
}

int terminateRotate(Ev3SeqParam param)
{
	// 指定角度が0のときは無条件でTrue
	if(param.encoder == 0){
		return TRUE;
	}
	// 総回転角度が指定角度を超えたら終了
	if (abs_f(direction) >= abs_i(param.encoder)-ANGLE_CORRECTION)
	{
		return TRUE;
	}
	return FALSE;
}

/*
 ******************************************** Line Return *********************************************
*/
int beforeLineReturn(){
	color_sensor_mode = LUMINANCE_MODE;				// カラーセンサを輝度モードにする
	distance = 0.0;									// 距離を初期化
	pre_angleL = ev3_motor_get_counts(left_motor);	// モータの過去値を初期化
    pre_angleR = ev3_motor_get_counts(right_motor);
	line_return_state = BEFORE_BLACK_DETECTION;		// ライン検知状態を初期化
	before_black_dist = 0.0;
	after_black_dist = 0.0;
    return TRUE;
}

int execLineReturn(Ev3SeqParam param)
{
	// モーター駆動
	ev3_motor_set_power(left_motor, param.speed);
	ev3_motor_set_power(right_motor, param.speed);
	// 距離更新
	distanceUpdate();

	// ライン検知状態を更新
	lineDetectUpdate();
	return_edge = param.edge;			// 本当はafterLineReturn()で判定したいけど、全体的に書き換えるのが面倒なのでとりあえずここで指定

    return TRUE;
}

int afterLineReturn(){
	float passage_distance = after_black_dist - before_black_dist;
	float rotate_angle = asin(20.0/passage_distance)* 180.0 / PI;

	char dstr[20];
	char astr[20];
	sprintf(dstr, "dist : %f ", passage_distance);
	sprintf(astr, "angle : %f ", rotate_angle);
	ev3_lcd_draw_string(dstr, 0, CALIB_FONT_HEIGHT*5);
	ev3_lcd_draw_string(astr, 0, CALIB_FONT_HEIGHT*6);

	int set_param = (int)(630.0*rotate_angle/360.0);

	// 左に復帰したいとき
	if(return_edge == EDGE_LEFT){
		ev3_motor_rotate(right_motor, -1*set_param, 40, false);
		ev3_motor_rotate(left_motor, set_param, 40, true);
	}
	// 右に復帰したいとき
	else{
		ev3_motor_rotate(right_motor, set_param, 40, false);
		ev3_motor_rotate(left_motor, -1*set_param, 40, true);
	}

	 return TRUE;
}

int terminateLineReturn(Ev3SeqParam param)
{
    if (line_return_state == AFTER_WHITE_DETECTION)
	{
         return TRUE;
    }
    return FALSE;
}

/*
** Sub functions
*/
/* 距離更新（3ms間の移動距離を毎回加算している） */
void distanceUpdate(){
    float cur_angleL = ev3_motor_get_counts(left_motor); //左モータ回転角度の現在値
    float cur_angleR = ev3_motor_get_counts(right_motor);//右モータ回転角度の現在値

    // 3ms間の走行距離 = ((円周率 * タイヤの直径) / 360) * (モータ角度過去値　- モータ角度現在値)
    distanceL = ((PI * TIRE_DIAMETER) / 360.0) * (cur_angleL - pre_angleL);  // 4ms間の左モータ距離
    distanceR = ((PI * TIRE_DIAMETER) / 360.0) * (cur_angleR - pre_angleR);  // 4ms間の右モータ距離
    distance += (distanceL + distanceR) / 2.0;

    //モータの回転角度の過去値を更新
    pre_angleL = cur_angleL;
    pre_angleR = cur_angleR;

	return;
}

/* 角度更新 */
void directionUpdate(){
	//(360 / (2 * 円周率 * 車体トレッド幅)) * (右進行距離 - 左進行距離)
	direction += (360.0 / (2.0 * PI * TREAD)) * (distanceL - distanceR);
}

/* ライン検知状態を更新 */
void lineDetectUpdate(){
	// 黒検知前状態で黒を検知した場合
	if(line_return_state==BEFORE_BLACK_DETECTION && ltvalue.ref_value<ltvalue.threshold_value){
		line_return_state = AFTER_BLACK_DETECTION;
		before_black_dist = distance;
	}

	// 黒検知後状態で白を検知した場合
	if(line_return_state==AFTER_BLACK_DETECTION && ltvalue.ref_value>ltvalue.threshold_value){
		line_return_state = AFTER_WHITE_DETECTION;
		after_black_dist = distance;
	}
}

void toBoard() {
	ev3_motor_rotate(arm_motor, 60, 50, true);
	ev3_motor_rotate(left_motor, 560, 50, false);
	ev3_motor_rotate(right_motor, 560, 50, true);
	ev3_motor_rotate(arm_motor, -60, 50, true);
}

void train_wait() {
	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);
	tslp_tsk(50);
	while (40 < sensor_distance) {
		tslp_tsk(10); //ソナータスクを走らせる
	}
	tslp_tsk(1500); //電車の通過待ち
}

int16_t max(int16_t a,int16_t b){
  return a<b?b:a;
}

int16_t min(int16_t a,int16_t b){
  return a<b?a:b;
}

float round_e(float num){
  return num;
}

// RGBをHSVに変換する（20170708 kashiwaba）
hsv_raw_t convertRGB2HSV(rgb_raw_t src)
{
    int r,g,b;
    float nr,ng,nb;
    float h,s,v;
    int min_color = 0; // 0:R 1:G 2:B
    float max_num,min_num;
	hsv_raw_t dst;

    // GET RGB
    r = src.r;
    g = src.g;
    b = src.b;

    // GET NORMALIZED RGB
    nr = (float)r/255;
    ng = (float)g/255;
    nb = (float)b/255;

    // CALC MAX,MIN,MIN_COLOR
    max_num = (float)(fmax(fmax(r,g),b))/255;
    min_num = (float)(fmin(fmin(r,g),b))/255;

    if(r<g && r<b)
        min_color = 0;
    else if(g<r && g<b)
        min_color = 1;
    else
        min_color = 2;


    // calc HSV
    s = max_num - min_num;
    v = max_num;
    // 最大値、最小値が同じ
    if(min_num == max_num){
        h = 0;
    }
    // Rが最小
    else if(min_color == 0){
        h = 60*(nb-ng)/(max_num-min_num)+180;
    }
    // Gが最小
    else if(min_color == 1){
        h = 60*(nr-nb)/(max_num-min_num)+300;
    }
    // Bが最小
    else{
        h = 60*(ng-nr)/(max_num-min_num)+60;
    }

    dst.h = h;
    dst.s = s;
    dst.v = v;

	return dst;
}

// HSVから床の色を判定（20170708 kashiwaba）
colorid_t get_floor_color(sensor_port_t port){
	rgb_raw_t rgb;
	hsv_raw_t hsv;
	char hstr[20];
    char sstr[20];
    char vstr[20];
    ev3_color_sensor_get_rgb_raw(port,&rgb);
	hsv = convertRGB2HSV(rgb);

    float h,s,v;

	h = hsv.h;
	s = hsv.s;
	v = hsv.v;

	sprintf(hstr, "H : %f ", h);
    sprintf(sstr, "S : %f ", s);
    sprintf(vstr, "V : %f ", v);

	 ev3_lcd_draw_string(hstr, 0, CALIB_FONT_HEIGHT*5);
	 ev3_lcd_draw_string(sstr, 0, CALIB_FONT_HEIGHT*6);
     ev3_lcd_draw_string(vstr, 0, CALIB_FONT_HEIGHT*7);

    // 黒検出
	if (v < color_param[4]){
        return COLOR_BLACK;
    }

    // H値による　R/YW/G/B判定
	// Red
	if (h <= color_param[0]){
		return COLOR_RED;
	}
	// Yellow or White
	else if (color_param[0]<h && h<color_param[2]){
		// S値によるY/W判定
		// Yellow
		if (s > color_param[1]){
			return COLOR_YELLOW;
		}
		// White
		else{
			return COLOR_WHITE;
		}
	}
	// Green
	else if (color_param[2]<=h && h<color_param[3] && s>color_param[6]){
		return COLOR_GREEN;
	}
	// Blue
	else if (h>=color_param[3] && s<color_param[6]){
		return COLOR_BLUE;
	}
	else{
		return COLOR_NONE;
	}
}

//rgb値の生データをhsv値に変換
rgb_raw_t conv_hsv_hue(rgb_raw_t rgb){
  int16_t max_v = max(max(rgb.r,rgb.g),rgb.b);
  int16_t min_v = min(min(rgb.r,rgb.g),rgb.b);
  float h=0,s=0,v=0;
  rgb_raw_t ret;
  v=max_v;
  if (max_v != min_v) {
    if (max_v == rgb.r){
      h = 60 * (rgb.g - rgb.b) / (max_v-min_v);
    }
    if (max_v == rgb.g){
      h = 60 * (rgb.b - rgb.r) / (max_v-min_v) + 120;
    }
    if (max_v == rgb.b){
      h = 60 * (rgb.r - rgb.g) / (max_v-min_v) + 240;
    }
    s=((max_v-min_v)*100)/(max_v*100);
  }

  if (h < 0){
    h = h + 360;
  }
  ret.r = (int16_t)round_e(h);
  ret.g = (int16_t)round_e(s * 100);
  ret.b = (int16_t)round_e((v / 255) * 100);
  return ret;
}

//hsv値から色を返す
colorid_t ev3_color_sensor_get_color_nice(sensor_port_t port){
  rgb_raw_t rgb;
  rgb_raw_t hsv;
  ev3_color_sensor_get_rgb_raw(port,&rgb);
  hsv=conv_hsv_hue(rgb);

  if(hsv.b<30){
    return COLOR_BLACK;
  }
  if(hsv.r<83){
    if(hsv.r<35){
      return COLOR_RED;
    }else{
      return ev3_color_sensor_get_color(port); //黄色と白はAPIを利用 hsv
    }
  }else{
    if(rgb.b<60){
      return COLOR_GREEN;
    }else{
      return COLOR_BLUE;
    }
  }
}

//引数に進ませたい距離を入力
int distance_cal(int8_t distance) {
	return (360 * distance) / 25;
}

//進ませたい距離とスピードを入力
void xcm_GO(int8_t distance, uint32_t speed) {
	ev3_motor_rotate(left_motor, distance_cal(distance), speed, false);  //xcm前進
	ev3_motor_rotate(right_motor, distance_cal(distance), speed, true);
}
void xcm_GO_ver2(int8_t distance, uint32_t speed) {
	int count_init = ev3_motor_get_counts(right_motor);
	while(abs(ev3_motor_get_counts(right_motor) - count_init) < distance_cal(distance)){
		ev3_motor_steer(left_motor,right_motor,speed,0);
	}
}

//角位置リセット
void tire_position_reset(void) {
	ev3_motor_reset_counts(left_motor);
	ev3_motor_reset_counts(right_motor);
}

void oshidashi() {
	xcm_GO(7,30);
	ev3_motor_rotate(arm_motor,60,40,true);
	ev3_motor_rotate(arm_motor,-60,40,false);
	xcm_GO(-7,30);

	ev3_motor_rotate(left_motor, 288, 40, false);      //180°ターン
	ev3_motor_rotate(right_motor, -288, 40, true); //タイヤをそれぞれ違う向きに288°回す



	tire_position_reset();  //角位置リセット
}

void iron_tail(int8_t num) {
	switch(num){
	case ODD:
		ev3_motor_rotate(left_motor, 318, 40, false); //180°+α右ターン(両輪を逆向き288°回すと180°回る)
		ev3_motor_rotate(right_motor, -318, 40, true);

		ev3_motor_rotate(left_motor, -30, 35, false);      //+αの分を戻す
		ev3_motor_rotate(right_motor, 30, 35, true);
		break;
	case EVEN:
		ev3_motor_rotate(right_motor, 318, 40, false); //180°+α左ターン(両輪を逆向き288°回すと180°回る)
		ev3_motor_rotate(left_motor, -318, 40, true);

		ev3_motor_rotate(left_motor, -40, 35, false);      //復帰安定向上
		ev3_motor_rotate(right_motor, 40, 35, true);
		break;
	}
	tire_position_reset();  //角位置リセット
}

void color_catch(void) {
	static int color_count = 0; //色検知回数
	static colorid_t block_color = COLOR_NONE;

	//両輪の角位置リセット
	tire_position_reset();
	//5cm後退
	xcm_GO(-5, SPEED_BOARD);
	//ブロックを読み取る位置までアームを上げる
	ev3_motor_rotate(arm_motor, ARM_UP, 100, true);
	//角位置リセット
	tire_position_reset();
	block_color = ev3_color_sensor_get_color(color_sensor);
	//色検知するまで前進
	while ((block_color != COLOR_BLUE) && (block_color != COLOR_RED)
			&& (block_color != COLOR_YELLOW) && (block_color != COLOR_GREEN)) {
		block_color = ev3_color_sensor_get_color(color_sensor);
		ev3_motor_steer(left_motor, right_motor, 10, 0);
		//ブロックの色を検知しないで10cm進んだら強制的にループを抜ける
		if ((ev3_motor_get_counts(left_motor) > distance_cal(7))
				|| (ev3_motor_get_counts(right_motor) > distance_cal(7))) {
			block_color = COLOR_NONE;
			break;
		}
	}
	//停止
	ev3_motor_stop(left_motor, true);
	ev3_motor_stop(right_motor, true);
	if (floor_color == block_color) {
		iron_tail((color_count+1)%2);//偶数か奇数かで引数を変更
		ev3_motor_rotate(arm_motor, -ARM_UP, 100, false);//アームを戻す
	} else {
		ev3_motor_rotate(arm_motor, -ARM_UP, 100, true);//アームを戻す
		oshidashi();
	}
	color_count++;

	//カラーブロックを処理した回数別の処理
	switch (color_count) {
	case 1:
	case 3:
		ev3_motor_stop(left_motor, true);  //ここで止める動作をしないと3回目に謎の挙動をすることがある
		ev3_motor_stop(right_motor, true);
		xcm_GO(14, 40);  //13cm進む（十字路を超えるため）
		ev3_motor_rotate(left_motor, -30, 20, false); //ライントレース復帰安定向上
		ev3_motor_rotate(right_motor, 30, 20, true);
		break;
	case 2:
		ev3_motor_rotate(left_motor, -30, 20, false); //ライントレース復帰安定向上
		ev3_motor_rotate(right_motor, 30, 20, true);
		break;
	case 4:
		ev3_motor_rotate(left_motor, -30, 20, false); //ライントレース復帰安定向上
		ev3_motor_rotate(right_motor, 30, 20, true);
		psvalue.exit_flag = 1;
		ev3_led_set_color(LED_OFF);
		color_count = 0;
		break;
	default:
		break;
	}
}

//ファンファーレ
void fanfale(void){

	ev3_speaker_play_tone(NOTE_C5,142);
	tslp_tsk(152);
	ev3_speaker_play_tone(NOTE_C5,142);
	tslp_tsk(152);
	ev3_speaker_play_tone(NOTE_C5,142);
	tslp_tsk(152);
	ev3_speaker_play_tone(NOTE_C5,428);
	tslp_tsk(438);
	ev3_speaker_play_tone(NOTE_GS4,428);
	tslp_tsk(438);
	ev3_speaker_play_tone(NOTE_AS4,428);
	tslp_tsk(438);
	ev3_speaker_play_tone(NOTE_C5,142);
	tslp_tsk(284);
	ev3_speaker_play_tone(NOTE_AS4,142);
	tslp_tsk(152);
	ev3_speaker_play_tone(NOTE_C5,1284);
	tslp_tsk(1294);

	//while(1){
	/*	ev3_speaker_play_tone(NOTE_G4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_F4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_G4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_F4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_AS4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_AS4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_A4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_AS4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_A4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_A4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_G4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_F4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_E4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_F4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_D4,1926);
		tslp_tsk(1936);

		ev3_speaker_play_tone(NOTE_G4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_F4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_G4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_F4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_AS4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_AS4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_A4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_AS4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_A4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_A4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_G4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_F4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_G4,428);
		tslp_tsk(438);
		ev3_speaker_play_tone(NOTE_AS4,214);
		tslp_tsk(224);
		ev3_speaker_play_tone(NOTE_C5,1926);
		tslp_tsk(1936);*/
	//}
}

void fanfale2(){//クロノトリガー　ルッカのテーマ　♪=461
	ev3_speaker_play_tone(NOTE_E4,461/3);
	tslp_tsk(461/3 * 2 + 10);
	ev3_speaker_play_tone(NOTE_E4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_E4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_D4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_E4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_F4,461/3);
	tslp_tsk(461/3 * 2 + 10);
	ev3_speaker_play_tone(NOTE_F4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_F4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_E4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_F4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_G4,461/3);
	tslp_tsk(461/3 * 2 + 10);
	ev3_speaker_play_tone(NOTE_C5,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_C5,461*2);
	tslp_tsk(461*2 + 10);
	ev3_speaker_play_tone(NOTE_AS4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_AS4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_AS4,461/3);
	tslp_tsk(461/3 + 10);
	ev3_speaker_play_tone(NOTE_C5,461*4);
	tslp_tsk(461*4+10);

	//while(1){
/*		ev3_speaker_play_tone(NOTE_C5,461);
		tslp_tsk(461+10);
		ev3_speaker_play_tone(NOTE_C4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_F4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_G4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_C5,461);
		tslp_tsk(461+10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 * 2 + 10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 * 2 + 10);
		ev3_speaker_play_tone(NOTE_C5,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_G4,461*3);
		tslp_tsk(461*3+10);

		ev3_speaker_play_tone(NOTE_C5,461);
		tslp_tsk(461+10);
		ev3_speaker_play_tone(NOTE_C4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_F4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_G4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_C5,461);
		tslp_tsk(461+10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 * 2 + 10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_C5,461/3);
		tslp_tsk(461/3 * 2 + 10);
		ev3_speaker_play_tone(NOTE_E5,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_E5,461*2);
		tslp_tsk(461*2 + 10);

		ev3_speaker_play_tone(NOTE_F5,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_E5,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_D5,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_C5,461);
		tslp_tsk(461+10);
		ev3_speaker_play_tone(NOTE_C4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_F4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_G4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_C5,461);
		tslp_tsk(461+10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 * 2 + 10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_B4,461/3);
		tslp_tsk(461/3 * 2 + 10);
		ev3_speaker_play_tone(NOTE_C5,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_G4,461*2);
		tslp_tsk(461 * 2 + 10);

		ev3_speaker_play_tone(NOTE_F4,461);
		tslp_tsk(461 + 10);
		ev3_speaker_play_tone(NOTE_C4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_D4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_DS4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_D4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_DS4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_F4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_DS4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_F4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_G4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_F4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_G4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_AS4,461/3);
		tslp_tsk(461/3 + 10);
		ev3_speaker_play_tone(NOTE_C5,461*4);
		tslp_tsk(461*4+10);*/
	//}
}
