/**
 * API sample program.
 *
 * Robot construction: Educator Vehicle
 *
 * References:
 * http://robotsquare.com/wp-content/uploads/2013/10/45544_educator.pdf
 * http://thetechnicgear.com/2014/03/howto-create-line-following-robot-using-mindstorms/
 */

#include "ev3api.h"
#include "app.h"
#include "variable.h"
#include "run.h"
#include <syssvc/serial.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define DEBUG

#ifdef DEBUG
#define _debug(x) (x)
#else
#define _debug(x)
#endif

/*
* Global Variables
*/
Ev3SeqStep *ev3ExecTbl;
int TblSize;

/*
* Functions
*/
int8_t absw(int_t n) {   //絶対値を返す関数
	return n < 0 ? -1 * n : n;
}

int8_t get_gray_value(void) {
	static rgb_raw_t rgb;
	ev3_color_sensor_get_rgb_raw(color_sensor, &rgb);
	return (rgb.r * 0.299 + rgb.g * 0.587 + rgb.b * 0.114) * 0.39;
}

int8_t contactValidator() {
	if (0 < absw(ev3_motor_get_counts(arm_motor))) {
		return 1;
	} else {
		return 0;
	}
}

void arm_init(void) {
	//アームを一番上まで上げる
	ev3_motor_set_power(arm_motor, 20);
	tslp_tsk(800);
	ev3_motor_stop(arm_motor, true);

	//アームを下げてその位置で初期化
	ev3_motor_rotate(arm_motor, -126, 15, true);
	ev3_motor_stop(arm_motor, true);
	ev3_motor_reset_counts(arm_motor); //モーターの現在位置を初期位置に
}

//back bottunを押すと止まる
void button_clicked_handler(intptr_t button) {
    switch(button) {
    case BACK_BUTTON:
    	ev3_lcd_draw_string("BACK_BOTTUN_PRESSED", 0, CALIB_FONT_HEIGHT*1);
		ev3_motor_stop(left_motor, true);
		ev3_motor_stop(right_motor, true);
		ev3_stp_cyc(MOTOR_TASK);
		ev3_stp_cyc(SENSOR_TASK);
		ev3_stp_cyc(MONITOR_TASK);
		ev3_stp_cyc(CONTACT_TASK);
		ev3_stp_cyc(SONOR_TASK);
        while(1);
        break;
    }
}

// パラメータファイルを読み込む
void loadFile(void) {
	FILE *fp;
	char *filename = "param.txt";
	char readline[256] = { '\0' };
	char *param_name, *param_val_str;
	int list_num = sizeof param_list / sizeof param_list[0];
	int i;

	if ((fp = fopen(filename, "r")) == NULL) {
		ev3_lcd_draw_string("ERROR", 0, CALIB_FONT_HEIGHT*1);
		return;
	}

	// 1行ずつ読み込み
	while (fgets(readline, 256, fp) != NULL) {
		// ":"でパース
		param_name = strtok(readline, ":");
		param_val_str = strtok(NULL, ":");
		// param_listに登録された変数なら、color_paramにセット
		for (i = 0; i<list_num; i++){
			if (strcmp(param_list[i], param_name) == 0){
				color_param[i] = atof(param_val_str);
			}
		}
	}

	fclose(fp);
	return;
}

Ev3SeqParam setParam(int encoder, int speed, int edge, float p, float i, float d){
    Ev3SeqParam param;
 
    param.encoder = encoder;
    param.speed = speed;
    param.edge = edge;
    param.p = p;
    param.i = i;
    param.d = d;
 
    return param;
}
 
void push(int index, int(*beforeFunc)(Ev3SeqParam param), int(*executeFunc)(Ev3SeqParam param), Ev3SeqParam param, int(*afterFunc)(), int(*terminateFunc)(), int *jumpResult, int jumpIndex){
    TblSize += 1;
    ev3ExecTbl = (Ev3SeqStep *)realloc(ev3ExecTbl, sizeof(Ev3SeqStep)*TblSize);
 
    ev3ExecTbl[TblSize - 1].index = index;
    ev3ExecTbl[TblSize - 1].beforeFunc = beforeFunc;
    ev3ExecTbl[TblSize - 1].executeFunc = executeFunc;
    ev3ExecTbl[TblSize - 1].param = param;
    ev3ExecTbl[TblSize - 1].afterFunc = afterFunc;
    ev3ExecTbl[TblSize - 1].terminateFunc = terminateFunc;
    ev3ExecTbl[TblSize - 1].jumpResult = jumpResult;
    ev3ExecTbl[TblSize - 1].jumpIndex = jumpIndex;
 
    return;
}

/**
* @brief		ブロック並べのノード間移動の走行処理をテーブルに追加する関数
* @param[in]	method			始点ノードと終点ノードの種類（variable.hで定義したBlockAlignMethodを使う）
* @param[in]	have_block		ブロックを持っているかどうか（持っている：TRUE、持っていない：FALSE）
* @param[in]	angle			旋回する角度
* @param[in]	dist			通常ノード->中間ノードへ移動する際の走行距離（M2N以外のときは参照しないので適当な値を設定すれば良い）
* @return		void
* @details		コード読めば分かる
*/
void makeBlockAlignTable(int method, int have_block, int angle, int dist){
	// パラメータ
	int edge;
	int straight_speed;
	int trace_speed;
	int rotate_speed;
	float p = 2.0;				// 適当だから後でちゃんとした数値にする
	float i = 0.3;
	float d = 0.08;
	
	// 走行エッジを指定
	// 旋回角度が0°のとき
	if (angle == 0){
		// 基本的には直前の走行と同じエッジを走行する
		if (TblSize != 0){
			edge = ev3ExecTbl[TblSize - 1].param.edge;
		}
		// 多分ないけど一応エラー対策（デフォルト左走行）
		else{
			edge = EDGE_LEFT;
		}
	}
	// 旋回角度が180°かつブロックを保持していないとき
	else if (angle == 180 && !have_block){
		// 基本的には直前の走行と逆のエッジになる
		if (TblSize != 0){
			edge = (-1)*ev3ExecTbl[TblSize - 1].param.edge;
		}
		// 多分ないけど一応エラー対策（デフォルト左走行なのでその逆の右走行）
		else{
			edge = EDGE_RIGHT;
		}
	}
	// 旋回角度が正のとき
	else if (angle > 0){
		edge = EDGE_LEFT;
	}
	// 旋回角度が負のとき
	else{
		edge = EDGE_RIGHT;
	}


	// 走行スピードを設定						ToDo : 回転時にスピードを落とすのではなく曲率半径を変えたい(色々書き換えないといけなくて面倒なので後でやる)
	// ブロックを保持している場合
	if (have_block){
		straight_speed = 10;
		trace_speed = 10;
		rotate_speed = 10;
	}
	// ブロックを保持していない場合
	else{
		straight_speed = 30;
		trace_speed = 30;
		rotate_speed = 30;
	}


	// 始点ノードと終点ノードの種類毎にテーブルを作成する
	switch (method){
	case N2N:
		if (angle == 180 && !have_block){
			push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70, (-1)*straight_speed, edge, p, i, d), NULL, terminateEncoderBackMM, NULL, -1);	// 置いたブロックにぶつからないところまで下がる
			push(TblSize, beforeRotate, execRotate, setParam(angle, rotate_speed, edge, p, i, d), NULL, terminateRotate, NULL, -1);									// 回る
			push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(0, trace_speed, edge, p, i, d), NULL, terminateColor, NULL, -1);						// ライントレースで次ノードまで移動
		}
		else{
			push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70, straight_speed, edge, p, i, d), NULL, terminateEncoderMM, NULL, -1);				// 円の中心(より少し先)まで進む			
			push(TblSize, beforeRotate, execRotate, setParam(angle, rotate_speed, edge, p, i, d), NULL, terminateRotate, NULL, -1);									// 回る
			push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50, straight_speed, edge, p, i, d), NULL, terminateEncoderMM, NULL, -1);				// 円の外まで出る
			push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(0, trace_speed, edge, p, i, d), NULL, terminateColor, NULL, -1);						// ライントレースで次ノードまで移動
		}
		break;

	case N2M:
		// 後で作る
		// 多分N2Nとほとんど同じ（最後のColor->EncoderMMにするだけ？）
		break;
	case M2N:
		// 後で作る
		// 多分N2Nとほとんど同じ（最初の直進をなくして、最後のColor->EncoderMMにするだけ）
		break;
	case M2M:
		// 後で作る
		// ライン検知がバグってるので対応する必要あり
		break;
	default:
		break;
	}

}

/**
* Function Table
**/
/*
static Ev3SeqStep ev3ExecTbl[] =
{
	{0, beforeLineTrace, execLineTrace,  {1000,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08}, NULL, terminateEncoderMM, NULL, -1},
    {0, beforeStart, execStart,  {100,  10,  EDGE_RIGHT, 0.1, 0.01, 0.5}, afterStart, terminateStart, NULL, -1},
    {1, NULL, execOnboard,  {100,  10,  EDGE_RIGHT, 0.1, 0.01, 0.5}, NULL, terminateOnboard, NULL, -1},
    {2, beforeTrace, execTrace,  {100,  10,  EDGE_RIGHT, 0.1, 0.01, 0.5}, afterTrace, terminateTrace, NULL, -1},
	{3, beforePrize, execPrize,  {100,  10,  EDGE_RIGHT, 0.1, 0.01, 0.5}, afterPrize, terminatePrize, NULL, -1},
	{4, beforePutprize, execPutprize,  {100,  10,  EDGE_RIGHT, 0.1, 0.01, 0.5}, NULL, terminatePrize, NULL, -1},
	{5, beforeDown, execDown,  {100,  10,  EDGE_RIGHT, 0.1, 0.01, 0.5}, afterDown, terminateDown, NULL, -1},
    {6, NULL, execParking,  {100,  10,  EDGE_RIGHT, 0.1, 0.01, 0.5}, afterParking, terminateParking, NULL, -1},
    {7, beforeStop, execStop, {500,  100, EDGE_RIGHT, 0.1, 0.01, 0.5}, NULL, terminateStop, NULL, 0}
};
*/


/**
* Tasks
**/
void contact_task(intptr_t unused) {
	psvalue.isContact = contactValidator();
}

void sonor_task(intptr_t unused) {
	// Get sonor sensor value
	sensor_distance = ev3_ultrasonic_sensor_get_distance(sonor_sensor);
}

void sensing_task(intptr_t unused) {
	//カーブ判定
	static int is_power = 0; //左右のどちらのモーターに多くのパワーがかかっているか
	static int power_memo = 0; //is_powerの前の値
	static int left_power = 0;
	static int right_power = 0;

	left_power = ev3_motor_get_power(left_motor);
	right_power = ev3_motor_get_power(right_motor);

	is_power = left_power - right_power < 0; //左＞右 = 0(false)
	if (power_memo != is_power) {
		ccvalue.switch_curv = 1;
		ccvalue.curve_time = 0;
	} else {
		ccvalue.switch_curv = 0;
		ccvalue.curve_time++;
		if (CURVE_CHECK_VALUE < ccvalue.curve_time) {
			ccvalue.curve_count++;
		}
	}
	power_memo = is_power;

	if(color_sensor_mode == COLOR_MODE){
		ltvalue.ref_value = get_gray_value();
	}
	else if(color_sensor_mode == LUMINANCE_MODE){
		ltvalue_luminance.ref_value = ev3_color_sensor_get_reflect(color_sensor);
	}
}

void monitor_task(intptr_t unused) {
	int len = BUF_LEN;
	sprintf(buf, "\r\ndist:%d  ref:%d  ", sensor_distance,ltvalue.ref_value);
	serial_wri_dat(SIO_PORT_BT, buf, len);
}

void motor_task(intptr_t unused){
    // val
    int (*exec)(Ev3SeqParam);
    int (*before)();
    int (*after)();
    int (*terminate)(Ev3SeqParam);

    // get now table data
    Ev3SeqStep nowStep = ev3ExecTbl[motor_state];

    // exec before function
    if(!executedBeforeFunc && nowStep.beforeFunc!=NULL){
        before = nowStep.beforeFunc;
        executedBeforeFunc = (*before)();
    }

    // Exec
    exec = nowStep.executeFunc;
    (*exec)(nowStep.param);

    // Terminate
    terminate = nowStep.terminateFunc;
    if((*terminate)(nowStep.param)){
        // exec after function
        after = nowStep.afterFunc;
        if(after != NULL){
            (*after)();
        }

        // change state
        if(nowStep.jumpIndex < 0){
            motor_state++;
        }
        else{
            motor_state = nowStep.jumpIndex;
        }

        executedBeforeFunc = FALSE;
    }
}



void main_task(intptr_t unused){

	 // Register button handlers
	ev3_button_set_on_clicked(BACK_BUTTON, button_clicked_handler, BACK_BUTTON);


    // Configure sensors
	ev3_sensor_config(touch_sensor, TOUCH_SENSOR);
	ev3_sensor_config(color_sensor, COLOR_SENSOR);
	ev3_sensor_config(sonor_sensor, ULTRASONIC_SENSOR);

	// Configure motors
	ev3_motor_config(left_motor, LARGE_MOTOR);
	ev3_motor_config(right_motor, LARGE_MOTOR);
	ev3_motor_config(arm_motor, LARGE_MOTOR);
	ev3_motor_config(tail_motor, LARGE_MOTOR);
	arm_init();

	// Load File
	//loadFile();

    // Initialize global variables
    sensor_distance = 255;
    motor_state = run;
    executedBeforeFunc = FALSE;
	TblSize = 0;
    ev3ExecTbl = (Ev3SeqStep *)malloc(sizeof(Ev3SeqStep)*TblSize);
    state = start; //状態初期化

	// Make function table
	Ev3SeqParam DUMMY_PARAM = setParam(0,  0,  EDGE_RIGHT, 0.0, 0.0, 0.00);

	/*
	// 緑前->緑->青
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(90,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	*/
	// 緑前->緑->青（試してないけど↑と同じ処理になるはず）
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200, 30, EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	makeBlockAlignTable(N2N, FALSE, 90, 0);

	/*
	// 黄->青
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(30,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	*/

	/*
	// 黄->赤
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(-90,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	*/

	/*
	// 緑前->緑->青->赤->黄->緑
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);

	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(90,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);

	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(-90,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);

	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(-90,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);

	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(-90,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	*/

	/*
	// 直進ノード間
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(0,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(50,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderMM, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	*/

	/*
	// 180°回転
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_LEFT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	push(TblSize, beforeGoStraightAhead, execGoStraightAhead, setParam(70,  -30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateEncoderBackMM, NULL, -1);
	push(TblSize, beforeRotate, execRotate, setParam(180,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateRotate, NULL, -1);
	push(TblSize, beforeLineTraceColor, execLineTraceColor, setParam(200,  30,  EDGE_RIGHT, 2.0, 0.3, 0.08), NULL, terminateColor, NULL, -1);
	*/


	push(TblSize, beforeStop, execStop, DUMMY_PARAM, NULL, terminateStop, NULL, 0);

	int battery_V;
	int len = BUF_LEN;
    //バッテリーの電圧
    battery_V = ev3_battery_voltage_mV();
    sprintf(buf,"battery_mV:%d\r\n",battery_V);
    serial_wri_dat(SIO_PORT_BT,buf,len);
/*
    //キャリブレーション(カラーモード)
	color_sensor_mode = COLOR_MODE;
	do {
		ltvalue.black_value = get_gray_value();
		sprintf(buf, "BLACK = %d ", ltvalue.black_value);
		ev3_lcd_draw_string(buf, 0, CALIB_FONT_HEIGHT * 1);
	} while (!ev3_touch_sensor_is_pressed(touch_sensor));
	tslp_tsk(2500); //タッチセンサー長押し防止
	do {
		ltvalue.white_value = get_gray_value();
		sprintf(buf, "WHITE = %d ", ltvalue.white_value);
		ev3_lcd_draw_string(buf, 0, CALIB_FONT_HEIGHT * 1);
	} while (!ev3_touch_sensor_is_pressed(touch_sensor));
	ltvalue.threshold_value = (ltvalue.black_value + ltvalue.white_value) / 2;
	tslp_tsk(2000); //タッチセンサー長押し防止
	*/

	//キャリブレーション(輝度モード)
	color_sensor_mode = LUMINANCE_MODE;
	do {
		ltvalue_luminance.black_value = ev3_color_sensor_get_reflect(color_sensor);
		sprintf(buf, "BLACK = %d ", ltvalue_luminance.black_value);
		ev3_lcd_draw_string(buf, 0, CALIB_FONT_HEIGHT * 1);
	} while (!ev3_touch_sensor_is_pressed(touch_sensor));
	tslp_tsk(2500); //タッチセンサー長押し防止
	do {
		ltvalue_luminance.white_value = ev3_color_sensor_get_reflect(color_sensor);
		sprintf(buf, "WHITE = %d ", ltvalue_luminance.white_value);
		ev3_lcd_draw_string(buf, 0, CALIB_FONT_HEIGHT * 1);
	} while (!ev3_touch_sensor_is_pressed(touch_sensor));
	ltvalue_luminance.threshold_value = (ltvalue_luminance.black_value + ltvalue_luminance.white_value) / 2;
	tslp_tsk(2000); //タッチセンサー長押し防止

	while (!ev3_touch_sensor_is_pressed(touch_sensor)) {
		ev3_lcd_draw_string("PUSH TO START", 0, CALIB_FONT_HEIGHT * 1);
	};

    // start task
    ev3_sta_cyc(MOTOR_TASK);
	ev3_sta_cyc(SENSOR_TASK);
	ev3_sta_cyc(MONITOR_TASK);
	ev3_sta_cyc(CONTACT_TASK);
	ev3_sta_cyc(SONOR_TASK);

    return;
}
