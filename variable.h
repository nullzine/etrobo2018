#ifndef VARIABLE
#define VARIABLE

#include "ev3api.h"

/* LCDフォントサイズ */
#define CALIB_FONT (EV3_FONT_MEDIUM)
#define CALIB_FONT_WIDTH (6)
#define CALIB_FONT_HEIGHT (8)

#define EDGE_RIGHT (1)
#define EDGE_LEFT (-1)

#define TRUE (1)
#define FALSE (0)

#define PI 3.14159265358

#define BUF_LEN			100
//#define BOARD_VALUE		80		// color_param[6]として外部から読み込むように変更
#define TAPE_VALUE		90
#define CURVE_CHECK_VALUE	260
#define SPEED_BOARD		21	//板の上のスピード
#define ARM_UP 			86	//アームを上げる時の角度
#define EVEN			0	//偶数回目
#define ODD 			1	//奇数回目

/* State */
enum MotorState {
	run = 0, 
    stop
};
enum State {
	start, onboard, prize, down, parking
};

typedef struct
{
    int encoder;              /* エンコーダ値 */
    int speed;                /* スピード */
    int edge;                 /* ライントレースエッジ */
    float p;                    /* PID制御P */
    float i;                    /* PID制御I */
    float d;                    /* PID制御D */
}Ev3SeqParam;

typedef struct
{
    int index;                                  /* インデックス           */
    int (*beforeFunc)(Ev3SeqParam param);       /* 前処理関数             */
    int (*executeFunc)(Ev3SeqParam param);      /* 実行関数               */
    Ev3SeqParam param;                          /* 実行関数用パラメータ   */
    int (*afterFunc)();                         /* 後処理関数             */
    int (*terminateFunc)();                     /* 終了条件判定関数       */
    int *jumpResult;                            /* ジャンプ復帰値         */
    int jumpIndex;                              /* ジャンプ先インデックス */
}Ev3SeqStep;


//ライントレース用変数
enum ColorSensorState{
    LUMINANCE_MODE,
    COLOR_MODE
};

// ライン復帰用状態
enum LineReturnState{
    BEFORE_BLACK_DETECTION,
    AFTER_BLACK_DETECTION,
    AFTER_WHITE_DETECTION,
};

// ブロック並べ　ノード間移動方法
enum BlockAlignMethod{
	N2N,		// 通常ノード（Normal）-> 通常ノード（Normal）
	N2M,		// 通常ノード（Normal）-> 中間ノード（Mid）
	M2N,		// 中間ノード（Mid）-> 通常ノード（Normal）
	M2M,		// 中間ノード（Mid）-> 中間ノード（Mid）
};

typedef struct {
	int8_t black_value;
	int8_t white_value;
	int8_t threshold_value; //閾値
	int8_t ref_value; //反射値
} LinetraceValue;

//板の上り下り管理用変数
typedef struct {
	int8_t isContact;
	int8_t isOnPlane;
	int8_t exit_flag;
} PlateStateValue;

//カーブ判定用変数
typedef struct {
	int curve_count;
	int switch_curv;
	int curve_time;
} CurveCheckValue;

typedef struct {
	float h;
	float s;
	float v;
} hsv_raw_t;

/**
 * Touch sensor: Port 1
 * Color sensor: Port 2
 * Sonor sensor: Port 3
 * Gyro  sensor: Port 4

 * Color sensor motor	: Port A
 * Right motor			: Port B
 * Left  motor			: Port C
 * Tail motor			: Port D
 */
const int touch_sensor, color_sensor, sonor_sensor, gyro_sensor;
const int arm_motor, right_motor, left_motor, tail_motor;

extern int16_t sensor_distance;
extern enum MotorState motor_state;
extern int executedBeforeFunc;

extern LinetraceValue ltvalue;
extern LinetraceValue ltvalue_luminance;
extern PlateStateValue psvalue;
extern CurveCheckValue ccvalue;

extern colorid_t floor_color;
extern int erapsed_time; //経過時間
extern int time;
extern char buf[BUF_LEN];
extern int mStartClock;
extern enum State state; //状態
extern int16_t sensor_distance; //センサーの値

//*****************************************
//ETロボ追加
extern int boardflags;
extern char *param_list[7];
extern float color_param[7];
extern int color_sensor_mode;
//*****************************************


// run
extern int8_t steer_value; //ハンドルを切る値
extern int8_t prev; //1つ前の反射値
extern int8_t diff; //反射値の差
extern float integral; //誤差の累積

#endif /* VARIABLE */
