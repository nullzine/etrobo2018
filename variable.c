#include "ev3api.h"
#include "variable.h"

/*グローバル変数定義 */

/**
 * Touch sensor: Port 1
 * Color sensor: Port 2
 * Sonor sensor: Port 3
 * Gyro  sensor: Port 4

 * Color sensor motor	: Port A
 * Right motor			: Port B
 * Left  motor			: Port C
 * Tail motor			: Port D
 */
const int color_sensor = EV3_PORT_2, sonor_sensor = EV3_PORT_3;
const int right_motor = EV3_PORT_B, left_motor = EV3_PORT_C, tail_motor = EV3_PORT_D;

int16_t sensor_distance;
enum MotorState motor_state;
int executedBeforeFunc;

LinetraceValue ltvalue;                     // カラーセンサモード時の値
LinetraceValue ltvalue_luminance;           // 輝度センサモード時の値
PlateStateValue psvalue = { 0, 0, 0 };
CurveCheckValue ccvalue = { 0, 0, 0 };

colorid_t floor_color = COLOR_NONE;
int erapsed_time = 0; //経過時間
int time =0;
char buf[BUF_LEN];
int mStartClock;
enum State state; //状態
int16_t sensor_distance = 0; //センサーの値

int8_t steer_value; //ハンドルを切る値
int8_t prev; //1つ前の反射値
int8_t diff; //反射値の差
float integral; //誤差の累積

//************************************************
//ETロボ追加
int boardflags = 0;
char *param_list[7] = { "RED", "YELLOW", "GREEN", "BLUE_H", "BLACK", "BOARD", "BLUE_S" };
float color_param[7] = { 0 };
int color_sensor_mode = LUMINANCE_MODE;
//*****************************************
